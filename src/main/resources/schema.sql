create table User
(
userid varchar(100) not null,
password varchar(100),
secretanswer varchar(30),
role varchar(30),
primary key(userid)
);

create table student
(
   id IDENTITY  not null,
   name varchar(30) not null,
   userid varchar(30) not null,
   age integer,
   gender varchar(30),
   department varchar(100) not null,
   semester varchar(100) not null,
   primary key(id),
   FOREIGN KEY (userid) REFERENCES User(userid)
);

create table course
(
semester varchar(100) not null,
Department varchar(100) not null,
courseid varchar(30) not null,
coursename varchar(100) not null,
primary key(courseId)

);

