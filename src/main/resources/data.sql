insert into course (semester,department,courseid,coursename) VALUES ('Semester - 01','Computer Science','CS101','Mathematics-1');
insert into course (semester,department,courseid,coursename) VALUES ('Semester - 01','Computer Science','CS102','Basic Electrical and Electronics');
insert into course (semester,department,courseid,coursename) VALUES ('Semester - 01','Computer Science','CS103','Engineering graphics');
insert into course (semester,department,courseid,coursename) VALUES ('Semester - 01','Information and Technology','IT101','Mathematics-1');
insert into course (semester,department,courseid,coursename) VALUES ('Semester - 01','Information and Technology','IT102','Basic Electrical and Electronics');
insert into course (semester,department,courseid,coursename) VALUES ('Semester - 01','Information and Technology','IT103','Engineering graphics');
insert into course (semester,department,courseid,coursename) VALUES ('Semester - 02','Computer Science','CS201','Engineering Physics');
insert into course (semester,department,courseid,coursename) VALUES ('Semester - 02','Computer Science','CS202','Basic Mechanical Engineering');
insert into course (semester,department,courseid,coursename) VALUES ('Semester - 02','Computer Science','CS203','Basic Computer Engineering');
insert into course (semester,department,courseid,coursename) VALUES ('Semester - 02','Information and Technology','IT201','Engineering Physics');
insert into course (semester,department,courseid,coursename) VALUES ('Semester - 02','Information and Technology','IT202','Basic Mechanical Engineering');
insert into course (semester,department,courseid,coursename) VALUES ('Semester - 02','Information and Technology','IT203','Basic Computer Engineering');
insert into course (semester,department,courseid,coursename) VALUES ('Semester - 03','Computer Science','CS301','Mathematics-3');
insert into course (semester,department,courseid,coursename) VALUES ('Semester - 03','Computer Science','CS302','Electronic Devices and Circuits');
insert into course (semester,department,courseid,coursename) VALUES ('Semester - 03','Computer Science','CS303','Digital Circuits and Design');
insert into course (semester,department,courseid,coursename) VALUES ('Semester - 03','Information and Technology','IT301','Mathematics-3');
insert into course (semester,department,courseid,coursename) VALUES ('Semester - 03','Information and Technology','IT302','Digital Circuits and Systems');
insert into course (semester,department,courseid,coursename) VALUES ('Semester - 03','Information and Technology','IT303','Object Oriented Programming');



COMMIT;