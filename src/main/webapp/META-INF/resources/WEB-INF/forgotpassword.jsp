<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Forgot Password</title>

<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>
<script type="text/javascript">
	function validateDetails() {
		$.ajax({
			type : "GET",
			contentType : "application/json",
			url : "validate",
			data : {
				"userid" : $("#userid").val(),
				"petName" : $("#petName").val()

			},
			dataType : 'json',
			cache : false,
			timeout : 600000,
			success : function(data) {
				$('#resetPassword').html(data);

				console.log("SUCCESS : ", data);

			},
			error : function(e) {
				if(e.responseText == 'forgot')
					{
					$("#errorMessage").show();
					}
				if(e.responseText == 'reset')
				{
				$("#resetPassword").load("/resetPassword?userid="+$("#userid").val());
				}
				

				console.log("ERROR : ", e);

			}
		});

	}
</script>
<body>

	<div class="container">


		<form:form method="POST" class="form-signin">
			<div id="forgotPassword" align="left">
			<div id="errorMessage" style="display:none;">
				<span style="color: red">User Id/Secret Answer Invalid</span></div>
				<h2 class="form-signin-heading">Forgot Password</h2>

				User Id : <input type="text" class="form-control"
					placeholder="Username" autofocus="true" id="userid"></input> What's
				your pet's Name: <input type="text" class="form-control"
					placeholder="Pet Name" id="petName"></input>


				<button class="btn btn-lg btn-primary btn-block" type="button"
					onclick="validateDetails()" id="validate">Validate</button>
			</div>
			<div id="resetPassword" align="left">
			
			</div>
		</form:form>

	</div>

	<script src="${contextPath}/resources/js/jquery.min.js"></script>
	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
