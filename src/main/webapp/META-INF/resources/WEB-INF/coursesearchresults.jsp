<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Course Search</title>

<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>

<body>
		<form:form method="GET" modelAttribute="courseSearchForm"
			class="form-signin" >



			<div id="customerSearchesult">
			<c:if test="${courseSearchForm.courses ne null }">
			
			<table border="1">
			<tr><th>Course Id</th><th>Course Name</th></tr>
			<c:forEach var="course" items="${courseSearchForm.courses}">
			<tr><td><c:out value="${course.courseid}"></c:out></td><td><c:out value="${course.coursename }"/></td></tr>
			</c:forEach>
			</table>
			</c:if></div>
		</form:form>
</body>
</html>
