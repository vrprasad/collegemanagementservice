<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="_csrf" content="${_csrf.token}" />
<!-- default header name is X-CSRF-TOKEN -->
<meta name="_csrf_header" content="${_csrf.headerName}" />
<title>Course Search</title>

<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${contextPath}/resources/css/common.css" rel="stylesheet">
<script type="text/javascript">
	$(document).ready(function() {

		$("#courseSearchForm").submit(function(event) {

			//stop submit the form, we will post it manually.
			event.preventDefault();

			fire_ajax_submit();

		});

	});

	function fire_ajax_submit() {

		$.ajax({
			type : "GET",
			contentType : "application/json",
			url : "coursesearchresult",
			data : {
				"department" : $("#department").val(),
				"semester" : $("#semester").val()

			},
			dataType : 'json',
			cache : false,
			timeout : 600000,
			success : function(data) {

				$('#customerSearchesult').html(json);

				console.log("SUCCESS : ", data);

			},
			error : function(e) {

				var json = e.responseText;
				$('#customerSearchesult').html(json);

				console.log("ERROR : ", e);

			}
		});

	}
</script>
</head>

<body>


	<form:form method="GET" modelAttribute="courseSearchForm"
		class="form-signin" id="courseSearchForm">
		<spring:bind path="department">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				Department:
				<form:select path="department" id="department"
					items="${courseSearchForm.departmentList}" />

			</div>
		</spring:bind>
		<spring:bind path="semester">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				Semester:
				<form:select path="semester" id="semester"
					items="${courseSearchForm.semesterList}">
				</form:select>

			</div>
		</spring:bind>



		<button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
		<div id="customerSearchesult">
			<c:if test="${courseSearchForm.courses ne null }">

				<table>
					<tr>
						<th>Course Id</th>
						<th>Course Name</th>
					</tr>
					<c:forEach var="course" items="${courseSearchForm.courses}" varStatus="loop">
						<tr>
							<td><label id="courseId${loop.index}"><c:out value="${course.courseid}"></c:out></label></td>
							<td><label id="coursename${loop.index}"><c:out value="${course.coursename}" /></label></td>
						</tr>
					</c:forEach>
				</table>
			</c:if>
		</div>
	</form:form>


	<script src="${contextPath}/resources/js/jquery.min.js"></script>
	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
