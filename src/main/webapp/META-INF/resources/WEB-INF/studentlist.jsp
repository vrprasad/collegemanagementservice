<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Course Search</title>

<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>
<script type="text/javascript">
function removeStudent(id) {
alert("id"+id);
		$.ajax({
			type : "GET",
			contentType : "application/json",
			url : "deleteStudent",
			data : {
				"id" : id

			},
			dataType : 'json',
			cache : false,
			timeout : 600000,
			success : function(data) {

				$('#studentlistresult').html(json);

				console.log("SUCCESS : ", data);

			},
			error : function(e) {

				var json = e.responseText;
				$('#studentlistresult').html(json);

				console.log("ERROR : ", e);

			}
		});

	}
</script>
<body>
		<form:form method="GET" modelAttribute="students"
			class="form-signin" >



			<div id="studentlistresult">
			<c:if test="${students ne null }">
			
			<table border="1">
			<tr><th>Name</th><th>Department</th><th>Semester</th><th>Action</th></tr>
			<c:forEach var="student" items="${students}" varStatus="loop">
			<tr><td><label id="studentName${loop.index}"><c:out value="${student.name}"></c:out></label></td>
			<td><label  id="department${loop.index}"><c:out value="${student.department}"/></label></td>
			<td><label  id="semester${loop.index}"><c:out value="${student.semester}"/></label></td>
			<td><button id="deleteStudent${loop.index}" class="btn btn-lg btn-primary btn-block" type="button" onclick="removeStudent('<c:out value="${student.id}"/>')">Delete</button></td></tr>
			</c:forEach>
			</table>
			</c:if></div>
		</form:form>
</body>
</html>
