<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="_csrf" content="${_csrf.token}"/>
    <!-- default header name is X-CSRF-TOKEN -->
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
  <title>College Management Services</title>

  <!-- Bootstrap core CSS -->
  <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="${contextPath}/resources/css/simple-sidebar.css" rel="stylesheet">

</head>

<body>
<form:form id="logoutForm" method="POST" action="${contextPath}/logout"  modelAttribute="user">
  <div class="d-flex" id="wrapper">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="list-group list-group-flush">
        <a href="#" class="list-group-item list-group-item-action bg-light" id="home">Home</a>
        <a href="#" class="list-group-item list-group-item-action bg-light" id="courseSearch">Course Search</a>
        <c:if test="${user.role eq 'Admin' }">
        <a href="#" class="list-group-item list-group-item-action bg-light" id="studentlist">Student List</a>
        </c:if>
        <a onclick="document.forms['logoutForm'].submit()" class="list-group-item list-group-item-action bg-light" >Logout</a>
      </div>
    </div>


      <div class="container-fluid" id="content">
        <h2 class="form-heading">Welcome to College Management Services</h2>
        <p>The College Management services provides platform to faculty/Student.</p>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  </form:form>
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="${contextPath}/resources/js/jquery.min.js"></script>
  <script src="${contextPath}/resources/js/bootstrap.bundle.min.js"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#home").click(function(e) {
    	$("#content").load("home.jsp");
    });
    $("#courseSearch").click(function(e) {
    	$("#content").load("/coursesearch");
    });
    $("#studentlist").click(function(e) {
    	$("#content").load("/getStudents");
    });
    $("#logout").click(function(e) {
    	$("#content").load("/logout");
    });
  </script>

</body>

</html>
