<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Create an account</title>

<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>
<script type="text/javascript">

</script>
<body>

	<div class="container">
		

		<form:form method="POST" modelAttribute="registrationForm"
			class="form-signin">
			<div id="error" style="color:red">
			<form:errors path="student.user.userid"></form:errors><br/>
			<form:errors path="student.name"></form:errors><br/>
			<form:errors path="student.age"></form:errors><br/>
			<form:errors path="student.user.password"></form:errors><br/>
			<form:errors path="student.user.secretanswer"></form:errors><br/>
			<form:errors path="student.gender"></form:errors><br/>
			<form:errors path="student.department"></form:errors><br/>
			<form:errors path="student.semester"></form:errors><br/>
		</div>
			<h2 class="form-signin-heading">Create your account</h2>
			<spring:bind path="student.user.userid">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					User Id :
					<form:input type="text" path="student.user.userid"
						class="form-control" placeholder="Username" autofocus="true"
						id="userId" maxlength="30"></form:input>

				</div>
			</spring:bind>
			<spring:bind path="student.name">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					Name :
					<form:input type="text" path="student.name" class="form-control"
						placeholder="Student Name" id="name" maxlength="30"></form:input>

				</div>
			</spring:bind>
			<spring:bind path="student.age">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					Age :
					<form:input type="text" path="student.age" class="form-control"
						placeholder="Student age" id="age" maxlength="3"></form:input>

				</div>
			</spring:bind>

			<spring:bind path="student.user.password">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					Password:
					<form:input type="password" path="student.user.password"
						class="form-control" placeholder="Password" id="password" maxlength="30"></form:input>

				</div>
			</spring:bind>


			<spring:bind path="student.user.secretanswer">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					Pet Name:
					<form:input type="text" path="student.user.secretanswer"
						class="form-control" placeholder="Pet Name" id="petname" maxlength="30" ></form:input>

				</div>
			</spring:bind>

			<spring:bind path="student.gender">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					Gender:
					<form:select path="student.gender" id="gender">
						<form:option value="male" label="Male" />
						<form:option value="female" label="Female" />
					</form:select>

				</div>
			</spring:bind>
			<spring:bind path="student.department">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					Department:
					<form:select path="student.department" id="department"
						items="${registrationForm.departmentList}" />

				</div>
			</spring:bind>
			<spring:bind path="student.semester">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					Semester:
					<form:select path="student.semester" id="semester"
						items="${registrationForm.semesterList}">
					</form:select>

				</div>
			</spring:bind>



			<button class="btn btn-lg btn-primary btn-block" type="submit" id="register" >Submit</button>
		</form:form>

	</div>

	<script src="${contextPath}/resources/js/jquery.min.js"></script>
	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
