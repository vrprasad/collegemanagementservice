<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Forgot Password</title>

<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>
<script type="text/javascript">
	function validate() {
		var password = $("#password").val();
		var confirmpassword = $("#confirmPassword").val();

		if (password == confirmpassword) {
			var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,16}$/;
			if (password.match(passw)) {
				$("#resetPasswordForm").submit();

			} else {
				$("#resetMessage").show();
				$("#resetMessageWhenNotSame").hide();
			}

		} else {
			$("#resetMessageWhenNotSame").show();
			$("#resetMessage").hide();
		}

	}
</script>
<body>

	<div class="container">


		<form:form method="POST" modelAttribute="user"
			action="${contextPath}/resetPassword" class="form-signin"
			id="resetPasswordForm">
			<div id="resetMessage" style="display: none;">
				<span style="color: red">Password must contain at least one
					number, one special character, one uppercase, one lowercase letter,
					one special char and should contain 8 - 16 characters</span>
			</div>
			<div id="resetMessageWhenNotSame" style="display: none;">
				<span style="color: red">Password/Reset Password Should be
					Same</span>
			</div>
			<h2 class="form-signin-heading">Reset Password</h2>
			<form:hidden path="userid" />
			<form:hidden path="role" />
			<form:hidden path="secretanswer" />
			<spring:bind path="password">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					New Password :
					<form:input type="password" path="password" class="form-control"
						placeholder="New Password" autofocus="true" id="password"></form:input>

				</div>
			</spring:bind>

			<div class="form-group ${status.error ? 'has-error' : ''}">
				Confirm Password: <input type="password" class="form-control"
					placeholder="Confirm Password" id="confirmPassword"></input>

			</div>

			<button class="btn btn-lg btn-primary btn-block" type="button"
				onclick="validate()" id="reset">Reset</button>
		</form:form>

	</div>

	<script src="${contextPath}/resources/js/jquery.min.js"></script>
	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
