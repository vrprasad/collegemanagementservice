package com.poc.collegemanagement.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.poc.collegemanagement.model.RegistrationForm;
import com.poc.collegemanagement.service.UserService;

@Component
public class RegistrationValidator implements Validator {
	@Autowired
	private UserService userService;

	@Override
	public boolean supports(Class<?> aClass) {
		return RegistrationForm.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		RegistrationForm registrationForm = (RegistrationForm) o;
				
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "student.user.userid", "user.NotEmpty","Userid is mandatory.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "student.user.password", "password.NotEmpty","Password is mandatory.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "student.user.secretanswer", "petname.NotEmpty","Pet Name is mandatory.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "student.name", "name.NotEmpty","Name is mandatory.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "student.age", "age.NotEmpty","Age is mandatory.");


		if (registrationForm.getStudent().getName().length() < 3
				|| registrationForm.getStudent().getName().length() > 20) {
			errors.rejectValue("student.name", "Size.name","Name must be at least 3 - 20 characters long");
		}
		if (!(registrationForm.getStudent().getName().matches("[A-Za-z]+"))) {
			errors.rejectValue("student.name", "alpha.name","Name must contain letters only");
		}
		if (!(registrationForm.getStudent().getUser().getUserid().matches("[A-Za-z0-9]+"))
				|| registrationForm.getStudent().getUser().getUserid().length() < 6
				|| registrationForm.getStudent().getUser().getUserid().length() > 10) {
			errors.rejectValue("student.user.userid", "alphanumeric.userid","User Id must be alphanumeric and should contain 6 - 10 character");
		}
		if (!(isValidPassword(registrationForm.getStudent().getUser().getPassword()))
				|| registrationForm.getStudent().getUser().getPassword().length() < 8
				|| registrationForm.getStudent().getUser().getPassword().length() > 16) {
			errors.rejectValue("student.user.password", "validate.password","Password is invalid");
		}
		if (userService.findByUserid(registrationForm.getStudent().getUser().getUserid()) != null) {
			errors.rejectValue("username", "duplicateuser","User already exists ");
		}
		if (registrationForm.getStudent().getUser().getPassword().length() < 8
				|| registrationForm.getStudent().getUser().getPassword().length() > 16) {
			errors.rejectValue("student.user.password", "Size.userForm.password","Try one with at least 8 characters.");
		}
		if (!(registrationForm.getStudent().getUser().getSecretanswer().matches("[A-Za-z]+"))
				|| registrationForm.getStudent().getUser().getSecretanswer().length() < 3
				|| registrationForm.getStudent().getUser().getSecretanswer().length() > 20) {
			errors.rejectValue("student.user.secretanswer", "alpha.petname","Pet Name must be at least 3 - 10 characters long");
		}
		if (registrationForm.getStudent().getAge() < 18 || registrationForm.getStudent().getAge() > 120) {
			errors.rejectValue("student.age", "valid.age","Age should be greater than 18 and less than 120");
		}
	}
	public static boolean
    isValidPassword(String password) 
    { 
  
        // Regex to check valid password. 
        String regex = "^(?=.*[0-9])"
                       + "(?=.*[a-z])(?=.*[A-Z])"
                       + "(?=.*[@#$%^&+=])"
                       + "(?=\\S+$).{8,16}$"; 
  
        // Compile the ReGex 
        Pattern p = Pattern.compile(regex); 
  
        // If the password is empty 
        // return false 
        if (password == null) { 
            return false; 
        } 
  
        // Pattern class contains matcher() method 
        // to find matching between given password 
        // and regular expression. 
        Matcher m = p.matcher(password); 
  
        // Return if the password 
        // matched the ReGex 
        return m.matches(); 
    } 

}