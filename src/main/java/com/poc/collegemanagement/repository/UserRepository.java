package com.poc.collegemanagement.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.poc.collegemanagement.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

	User findByUserid(String username);
	
	User findByUseridAndSecretanswer(String userid,String seretanswer);
}
