package com.poc.collegemanagement.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.poc.collegemanagement.model.Course;

@Repository
public interface CourseRepository extends CrudRepository<Course, String>{

	public List<Course> findByDepartmentAndSemester(String department,String semester);
	
}
