package com.poc.collegemanagement.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.poc.collegemanagement.model.Student;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {

}
