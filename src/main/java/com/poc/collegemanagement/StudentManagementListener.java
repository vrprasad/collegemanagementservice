package com.poc.collegemanagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.poc.collegemanagement.model.User;
import com.poc.collegemanagement.service.UserService;

@Component
@Order(0)
class StudentManagementListener implements ApplicationListener<ApplicationReadyEvent> {

	@Autowired
	UserService userService;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		User user = userService.findByUserid("admin01");
		if (user == null) {
			user = new User("admin01", "AdminPass01@", "NA", "Admin");
			user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
			userService.saveUser(user);
		}
	}

}
