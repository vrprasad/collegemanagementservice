package com.poc.collegemanagement.model;

import java.util.Set;

public class RegistrationForm {
	private Student student;
	private Set<String> semesterList;
	private Set<String> departmentList;
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
	public Set<String> getSemesterList() {
		return semesterList;
	}
	public void setSemesterList(Set<String> semesterList) {
		this.semesterList = semesterList;
	}
	public Set<String> getDepartmentList() {
		return departmentList;
	}
	public void setDepartmentList(Set<String> departmentList) {
		this.departmentList = departmentList;
	}

}
