package com.poc.collegemanagement.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="course")
public class Course {

	@Id
	private String courseid;
	private String semester;
	private String department;
	private String coursename;
	public String getCourseid() {
		return courseid;
	}
	public void setCourseid(String courseid) {
		this.courseid = courseid;
	}
	public String getSemester() {
		return semester;
	}
	public void setSemester(String semester) {
		this.semester = semester;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getCoursename() {
		return coursename;
	}
	public void setCoursename(String coursename) {
		this.coursename = coursename;
	}
	public Course(String courseid, String semester, String department, String coursename) {
		super();
		this.courseid = courseid;
		this.semester = semester;
		this.department = department;
		this.coursename = coursename;
	}
	public Course() {
		super();
	}

}
