package com.poc.collegemanagement.model;

import java.util.List;
import java.util.Set;

public class CourseSearchForm {
	private String department;
	private String semester;
	private Set<String> departmentList;
	private Set<String> semesterList;
	
	private List<Course> courses;

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	public Set<String> getDepartmentList() {
		return departmentList;
	}

	public void setDepartmentList(Set<String> departmentList) {
		this.departmentList = departmentList;
	}

	public Set<String> getSemesterList() {
		return semesterList;
	}

	public void setSemesterList(Set<String> semesterList) {
		this.semesterList = semesterList;
	}

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}


}
