package com.poc.collegemanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.poc.collegemanagement.model.RegistrationForm;
import com.poc.collegemanagement.model.Student;
import com.poc.collegemanagement.model.User;
import com.poc.collegemanagement.service.CourseService;
import com.poc.collegemanagement.service.SecurityService;
import com.poc.collegemanagement.service.StudentService;
import com.poc.collegemanagement.service.UserService;
import com.poc.collegemanagement.validator.RegistrationValidator;

@Controller
public class RegistrationController {
	@Autowired
	UserService userService;

	@Autowired
	StudentService studentService;

	@Autowired
	CourseService courseServie;
	@Autowired
	private SecurityService securityService;
	@Autowired
	RegistrationValidator registrationValidator;

	@RequestMapping("/test")
	public String index() {

		Student student = new Student("Vijetha", 25, "female", "NA", "NA");
		student.setId(1);
		User user = new User("test6", "test6", "NA", "Admin");
		student.setUser(user);
		studentService.saveStudent(student);
		return "login";
	}

	@GetMapping("/registration")
	public ModelAndView registration(Model model) {
		RegistrationForm registrationForm = new RegistrationForm();
		registrationForm.setSemesterList(courseServie.getSemesters());
		registrationForm.setDepartmentList(courseServie.getDepartments());
		model.addAttribute("registrationForm", registrationForm);
		return new ModelAndView("registration", "registrationForm", registrationForm);
	}

	@PostMapping("/registration")
	public String registration(@ModelAttribute("registrationForm") RegistrationForm registrationForm,
			BindingResult bindingResult,Model model) {
		registrationValidator.validate(registrationForm, bindingResult);

		if (bindingResult.hasErrors()) {
			registrationForm.setSemesterList(courseServie.getSemesters());
			registrationForm.setDepartmentList(courseServie.getDepartments());
			model.addAttribute("registrationForm", registrationForm);
			return "registration";
		}
		registrationForm.getStudent().getUser().setRole("Student");
		studentService.saveStudent(registrationForm.getStudent());

		model.addAttribute("message", "User successfully registered. Login to continue");
		return "login";
	}

	@GetMapping("/login")
	public String login(Model model, String error, String logout) {
		if (error != null)
			model.addAttribute("error", "Your username and password is invalid.");

		if (logout != null)
			model.addAttribute("message", "You have been logged out successfully.");

		return "login";
	}

	@GetMapping({ "/", "/welcome" })
	public ModelAndView welcome(Model model) {
		String userId = securityService.findLoggedInUsername();
		User user = userService.findByUserid(userId);
		model.addAttribute("user", user);
		return new ModelAndView("welcome", "user", user);
	}

}
