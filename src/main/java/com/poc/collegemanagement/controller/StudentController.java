package com.poc.collegemanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.poc.collegemanagement.model.Student;
import com.poc.collegemanagement.service.CourseService;
import com.poc.collegemanagement.service.StudentService;
import com.poc.collegemanagement.service.UserService;
import com.poc.collegemanagement.validator.RegistrationValidator;

@Controller
public class StudentController {
	@Autowired
	UserService userService;

	@Autowired
	StudentService studentService;

	@Autowired
	CourseService courseServie;
	@Autowired
	RegistrationValidator registrationValidator;

		@GetMapping("/getStudents")
	public ModelAndView getStudents(Model model) {
		List<Student> students = studentService.findAll();

		return new ModelAndView("studentlist", "students", students);
	}

	@GetMapping("/deleteStudent")
	public ModelAndView deleteStudent(@RequestParam Long id, Model model) {
		studentService.deleteStudent(id);
		List<Student> students = studentService.findAll();
		return new ModelAndView("studentlist", "students", students);
	}



}
