package com.poc.collegemanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.poc.collegemanagement.model.CourseSearchForm;
import com.poc.collegemanagement.service.CourseService;
import com.poc.collegemanagement.service.StudentService;
import com.poc.collegemanagement.service.UserService;
import com.poc.collegemanagement.validator.RegistrationValidator;

@Controller
public class CourseController {
	@Autowired
	UserService userService;

	@Autowired
	StudentService studentService;

	@Autowired
	CourseService courseServie;
	@Autowired
	RegistrationValidator registrationValidator;

		@GetMapping("/coursesearch")
	public ModelAndView courseSearch(Model model) {
		CourseSearchForm courseSearchForm = new CourseSearchForm();
		courseSearchForm.setSemesterList(courseServie.getSemesters());
		courseSearchForm.setDepartmentList(courseServie.getDepartments());
		model.addAttribute("CourseSearchForm", courseSearchForm);
		return new ModelAndView("coursesearch", "courseSearchForm", courseSearchForm);
	}

	@GetMapping("/coursesearchresult")
	public ModelAndView getCourseSearchResult(@RequestParam String department, @RequestParam String semester,
			Model model) {

		CourseSearchForm courseSearchForm = new CourseSearchForm();
		courseSearchForm.setDepartment(department);
		courseSearchForm.setSemester(semester);

		courseSearchForm.setCourses(courseServie.findByDepartmentAndSemester(courseSearchForm.getDepartment(),
				courseSearchForm.getSemester()));
		model.addAttribute("CourseSearchForm", courseSearchForm);
		return new ModelAndView("coursesearchresults", "courseSearchForm", courseSearchForm);

	}





}
