package com.poc.collegemanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.poc.collegemanagement.model.User;
import com.poc.collegemanagement.service.CourseService;
import com.poc.collegemanagement.service.StudentService;
import com.poc.collegemanagement.service.UserService;
import com.poc.collegemanagement.validator.PasswordValidator;
import com.poc.collegemanagement.validator.RegistrationValidator;

@Controller
public class PasswordController {
	@Autowired
	UserService userService;

	@Autowired
	StudentService studentService;

	@Autowired
	CourseService courseServie;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	RegistrationValidator registrationValidator;
	@Autowired
	PasswordValidator passwordValidator;

	@GetMapping("/validate")
	@ResponseBody
	public String forgotPassword(@RequestParam String userid, @RequestParam String petName, Model model) {

		User user = userService.findByUseridAndSecretanswer(userid, petName);
		if (user != null) {
			return "reset";
		}
		model.addAttribute("message", "Invalid details");
		return "forgot";

	}

	@GetMapping("/resetPassword")
	public String resetPassword(@RequestParam String userid, Model model) {
		User user = userService.findByUserid(userid);
		user.setPassword("");
		model.addAttribute("user", user);
		return "resetpassword";

	}

	@GetMapping("/forgotPassword")
	public ModelAndView forgotPassword(Model model) {
		User user = new User();
		return new ModelAndView("forgotpassword", "user", user);
	}

	@PostMapping("/resetPassword")
	public String resetPassword(@ModelAttribute("user") User user, BindingResult bindingResult, Model model) {
		passwordValidator.validate(user, bindingResult);
		
		if (bindingResult.hasErrors()) {
			return "resetpassword";
		}
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userService.saveUser(user);
		model.addAttribute("message", "Reset Password is sucessful");

		return "login";
	}

}
