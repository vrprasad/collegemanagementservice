package com.poc.collegemanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages = {"/src/main/java/"})
@ComponentScan(basePackages = {"com.poc.collegemanagement"})
public class StudentManagementApplication {
	
	

	public static void main(String[] args) {
		
		SpringApplication.run(StudentManagementApplication.class, args);
	}

}
