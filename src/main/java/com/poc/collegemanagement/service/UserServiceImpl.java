package com.poc.collegemanagement.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.poc.collegemanagement.model.User;
import com.poc.collegemanagement.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	UserRepository userRepository;

	@Override
	public void saveUser(User user) {

		userRepository.save(user);

	}

	@Override
	public User findByUserid(String userid) {
		
		return userRepository.findByUserid(userid);
	}

	@Override
	public User findByUseridAndSecretanswer(String userid, String petName) {
		return userRepository.findByUseridAndSecretanswer(userid, petName);
	}


}
