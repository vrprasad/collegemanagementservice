package com.poc.collegemanagement.service;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.poc.collegemanagement.model.Course;

@Service
public interface CourseService {

	public Set<String> getDepartments();
	public Set<String> getSemesters();
	public List<Course> findByDepartmentAndSemester(String department,String semester);
}
