package com.poc.collegemanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.poc.collegemanagement.model.Student;
import com.poc.collegemanagement.model.User;
import com.poc.collegemanagement.repository.StudentRepository;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	StudentRepository studentRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public void saveStudent(Student student) {
		User user = student.getUser();
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		student.setUser(user);
		studentRepository.save(student);
	}

	@Override
	public void deleteStudent(Long id) {
		studentRepository.deleteById(id);
	}

	@Override
	public List<Student> findAll() {
		return (List<Student>) studentRepository.findAll();
		
	}

}
