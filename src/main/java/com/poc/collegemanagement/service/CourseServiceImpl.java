package com.poc.collegemanagement.service;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.poc.collegemanagement.model.Course;
import com.poc.collegemanagement.repository.CourseRepository;

@Service
public class CourseServiceImpl implements CourseService {
	@Autowired
	CourseRepository courseRepository;

	private static Iterable<Course> listOfCourses = null;
	private static Set<String> listOfDepartments = null;
	private static Set<String> listOfSemester = null;

	@Override
	public Set<String> getDepartments() {
		if (listOfDepartments == null) {
			setDepartmentAnSemester();

		}
		return listOfDepartments;

	}

	private void setDepartmentAnSemester() {
			listOfCourses = courseRepository.findAll();
		if (listOfCourses != null) {
			listOfDepartments = new TreeSet<String>();
			listOfSemester = new TreeSet<String>();
			for (Course course : listOfCourses) {
				listOfDepartments.add(course.getDepartment());
				listOfSemester.add(course.getSemester());
			}
		}
	}

	@Override
	public Set<String> getSemesters() {
		if (listOfSemester == null) {
			setDepartmentAnSemester();

		}
		return listOfSemester;
	}

	@Override
	public List<Course> findByDepartmentAndSemester(String department, String semester) {
		return courseRepository.findByDepartmentAndSemester(department, semester);
	}

}
