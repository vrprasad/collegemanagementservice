package com.poc.collegemanagement.service;

import org.springframework.stereotype.Service;

import com.poc.collegemanagement.model.User;

@Service
public interface UserService {
	
	public void saveUser(User user);

	public User findByUserid(String userid);
	
	public User findByUseridAndSecretanswer(String userid,String petName);


}
