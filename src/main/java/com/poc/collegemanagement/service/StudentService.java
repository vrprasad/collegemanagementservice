package com.poc.collegemanagement.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.poc.collegemanagement.model.Student;

@Service
public interface StudentService {

	public void saveStudent(Student student);
	public void deleteStudent(Long id);
	public List<Student> findAll();
}
